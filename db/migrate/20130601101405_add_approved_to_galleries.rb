class AddApprovedToGalleries < ActiveRecord::Migration
  def change
    add_column :galleries, :approved, :integer, :default => 0
  end
end

class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :full_name
      t.string :title
      t.text :description
      t.text :ingredient
      t.text :step

      t.timestamps
    end
  end
end

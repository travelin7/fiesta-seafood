class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :user_id
      t.string :title
      t.integer :like

      t.timestamps
    end
  end
end

class AddAttachmentMainImageProductImageIngredientImageCookImageToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :main_image
      t.attachment :product_image
      t.attachment :ingredient_image
      t.attachment :cook_image
    end
  end

  def self.down
    drop_attached_file :products, :main_image
    drop_attached_file :products, :product_image
    drop_attached_file :products, :ingredient_image
    drop_attached_file :products, :cook_image
  end
end

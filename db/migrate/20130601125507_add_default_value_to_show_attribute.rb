class AddDefaultValueToShowAttribute < ActiveRecord::Migration
  def change
    change_column :galleries, :like, :integer, :default => 0
  end
end

class AddImagesDimensionsToGallery < ActiveRecord::Migration
  def change
    add_column :galleries, :small_width, :integer, :default => 0
    add_column :galleries, :small_height, :integer, :default => 0
    add_column :galleries, :medium_width, :integer, :default => 0
    add_column :galleries, :medium_height, :integer, :default => 0
  end
end

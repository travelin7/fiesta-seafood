class Recipe < ActiveRecord::Base
  has_many :ratings
  
  attr_accessible :description, :full_name, :ingredient, :step, :title, :image, :approved
  
  validates :description, :presence => true
  validates :full_name, :presence => true
  validates :ingredient, :presence => true
  validates :step, :presence => true
  validates :title, :presence => true
  validates :image, :attachment_presence => true
  
  has_attached_file :image, :styles => { :small => "200*150>", :medium => "499x280>", :thumb => "100x100>" }, 
                            :default_url => "/images/:style/missing.png",
                            :url => "/system/:class/:attachment/:id_partition/:style/:basename.:extension",
                            :path => ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
  
  before_create :randomize_file_name
  
  private

  def randomize_file_name
    extension = File.extname(image_file_name).downcase
    self.image.instance_write(:file_name, "#{SecureRandom.hex(16)}#{extension}")
  end
  
end

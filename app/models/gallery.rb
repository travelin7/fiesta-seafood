class Gallery < ActiveRecord::Base
  attr_accessible :like, :title, :user_id, :image, :approved
  
  validates :image, :attachment_presence => true
  validates :title, :presence => true
  
  has_attached_file :image, :styles => { :thumb => "70*70", :small => "250*100>", :medium => "680*480>"}, 
                            :default_url => "/images/:style/missing.png",
                            :path => ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
  
  before_create :randomize_file_name
  
  private

  def randomize_file_name
    extension = File.extname(image_file_name).downcase
    self.image.instance_write(:file_name, "#{SecureRandom.hex(16)}#{extension}")
  end

end

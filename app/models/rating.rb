class Rating < ActiveRecord::Base
  belongs_to :recipe
  
  attr_accessible :rating, :recipe_id
  
  validates :rating, :presence => true
  validates :recipe_id, :presence => true
end

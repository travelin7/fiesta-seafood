class Product < ActiveRecord::Base
  attr_accessible :description, :title, :main_image, :product_image, :ingredient_image, :cook_image
  
  validates :title, :presence => true
  
  has_attached_file :main_image, :styles => { :thumb => "70*70", :medium => "109*149>" },
                                 :default_url => "/images/:style/missing.png",
                                 :path => ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
  
  has_attached_file :product_image, :styles => { :thumb => "70*70", :medium => "300*300>" },
                                    :default_url => "/images/:style/missing.png",
                                    :path => ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
                                    
  has_attached_file :ingredient_image, :styles => { :thumb => "70*70", :medium => "300*300>" },
                                       :default_url => "/images/:style/missing.png",
                                       :path => ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
                                       
  has_attached_file :cook_image, :styles => { :thumb => "70*70", :medium => "300*300>" },
                                 :default_url => "/images/:style/missing.png",
                                 :path => ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
end

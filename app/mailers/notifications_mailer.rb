class NotificationsMailer < ActionMailer::Base
  default :from => "noreply@fiestaseafood.com"
  default :to => "info@fiestaseafood.com"
  
  def new_message(message)
    @message = message
    mail(:subject => "[Fiestaseafood.com] A Message From #{message.name}")
  end
end

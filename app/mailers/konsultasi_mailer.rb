class KonsultasiMailer < ActionMailer::Base
  default :from => "noreply@fiestaseafood.com"
  default :to => "konsultasi@fiestaseafood.com"
  
  def new_message(message)
    @konsultasi = message
    mail(:subject => "[Fiestaseafood.com] Pertanyaan Konsultasi Dari #{message.name}")
  end
end

class BeritaController < ApplicationController
  before_filter :check_for_mobile, :only => [:show]
  
  layout "front"
  
  def index
    url = 'http://blog.fiestaseafood.com/api/index.php/category/9'
    
    @bodyid = "fiesta_berita"
    
    response = Nestful.get(url)
    response_decode = response.decoded
    
    @wordpress = JSON.parse(response_decode)
    
    respond_to do |format|
      format.html # index.html.erb
    end
  end
  
  def show
    @bodyid = "fiesta_berita"
    
    # get the news detail
    url = 'http://blog.fiestaseafood.com/api/index.php/post/'+params[:id]
    response = Nestful.get(url)
    response_decode = response.decoded
    @wordpress = JSON.parse(response_decode)
    
    # get the latest news
    url_latest = 'http://blog.fiestaseafood.com/api/index.php/category/9'
    latest_res = Nestful.get(url_latest)
    latest_decode = latest_res.decoded
    @latest = JSON.parse(latest_decode)
    
    respond_to do |format|
      format.html # show.html.erb
    end
  end
end

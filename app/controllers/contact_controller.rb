class ContactController < ApplicationController
  before_filter :check_for_mobile, :only => [:new]
  
  layout "front"
  
  def new
    @message = Message.new
  end
  
  def create
    @message = Message.new(params[:message])
    
    if @message.valid?
      NotificationsMailer.new_message(@message).deliver
      redirect_to(contact_path, flash[:notice] => "Message was successfully sent.")
    else
      flash[:notice] = "Please fill all fields."
      render :new
    end
  end
end

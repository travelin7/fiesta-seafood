class ProdukController < ApplicationController
  before_filter :check_for_mobile, :only => [:index]
  
  layout "front"
  
  def index
    @bodyid = "fiesta_produk"
    @products = Product.all
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end
end

class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :get_blog_posts
  
  def check_for_mobile
    session[:mobile_override] = params[:mobile] if params[:mobile]
    prepare_for_mobile if mobile_device?
  end
  
  def prepare_for_mobile
    prepend_view_path Rails.root + 'app' + 'views_mobile'
  end
  
  def mobile_device?
    if session[:mobile_override]
      session[:mobile_override] == "1"
    else
      request.user_agent =~ /Mobile|webOS/
    end
  end
  
  helper_method :mobile_device?
  
  private
    def get_blog_posts
      url = 'http://blog.fiestaseafood.com/api/index.php/category/8'
      response = Nestful.get(url)
      response_decode = response.decoded
      @blogs = JSON.parse(response_decode)
    end
end

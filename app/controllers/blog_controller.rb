class BlogController < ApplicationController
  before_filter :check_for_mobile, :only => [:index, :show]
  
  layout "front"
  
  def index
  end

  def show
  end
end

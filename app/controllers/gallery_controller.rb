class GalleryController < ApplicationController
  before_filter :check_for_mobile, :only => [:index]
  
  layout "front"
  
  def index
    @bodyid = "fiesta_galeri"
    
    @galleries = Gallery.find_all_by_approved(1);
    
    respond_to do |format|
      format.html
      format.json { render json: @galleries }
    end
  end
  
  def create
    @gallery = Gallery.new(:user_id => params[:user_id], :image => params[:image], :title => params[:title])
    
    respond_to do |format|
      if @gallery.save
        format.html { redirect_to gallery_index_path, notice: 'Your image has been saved.' }
      else
        format.html { render action: "index" }
      end
    end
  end
end

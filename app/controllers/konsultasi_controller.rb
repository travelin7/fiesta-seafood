class KonsultasiController < ApplicationController
  before_filter :check_for_mobile, :only => [:index, :new]
  
  layout "front"
  
  def index
    @bodyid = "fiesta_konsultasi"
    @konsultasi = Konsultasi.new
  end
  
  def new
    @bodyid = "fiesta_konsultasi"
    @konsultasi = Konsultasi.new
  end
  
  def create
    @konsultasi = Konsultasi.new(params[:konsultasi])
    
    if @konsultasi.valid?
      puts @konsultasi
      KonsultasiMailer.new_message(@konsultasi).deliver
      redirect_to(konsultasi_index_path, flash[:notice] => "Message was successfully sent.")
    else
      flash[:notice] = "Please fill all fields."
      render :new
    end
  end
  
end

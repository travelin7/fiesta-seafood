class AboutController < ApplicationController
  before_filter :check_for_mobile, :only => [:index]
  
  layout "front"
  
  def index
    @bodyid = "fiesta_tentang"
  end
end

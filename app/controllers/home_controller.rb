class HomeController < ApplicationController
  before_filter :check_for_mobile, :only => [:index]
  
  layout "front"
  # Default method for the home page
  #
  # @param none
  def index
    @bodyid = "fiesta_home"
    
    # if accessed from mobile
    if :mobile_device?
      url = 'http://blog.fiestaseafood.com/api/index.php/category/9'
      response = Nestful.get(url)
      response_decode = response.decoded
      
      @wordpress = JSON.parse(response_decode)

    end
  end
end

class LocationController < ApplicationController
  before_filter :check_for_mobile, :only => [:index]
  
  layout "front"
  
  def index
  end
end

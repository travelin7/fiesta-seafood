class Manage::DashboardController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  
  # The home of dashboard panel
  #
  # @param none
  def index
    
  end
end

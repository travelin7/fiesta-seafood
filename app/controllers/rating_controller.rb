class RatingController < ApplicationController
  
  # Post the rating
  def create
    check_rating = Rating.where(:recipe_id => params[:id])
      
    if check_rating.count > 0
      #if the rating exist
      @rating = Rating.find_by_recipe_id(params[:id])
      @rating.rating = @rating.rating + params[:value].to_i
    else
      #if the rating not exist
      @rating = Rating.new(:recipe_id => params[:id], :rating => params[:value]);
    end
    
    respond_to do |format|
      if @rating.save
        format.json { render json: @rating, status: :created }
      end
    end
  end

end
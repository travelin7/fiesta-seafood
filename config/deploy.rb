set :application, "Fiesta web"
set :repository,  "ssh://root@113.20.31.104/var/www/gitrepo/fiesta"

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :deploy_to, "/var/www/html/fiestaweb"
#set :deploy_to, "/home/fiestase/public_html"

#role :web, "fiestaseafood.com"                          # Your HTTP server, Apache/etc
#role :app, "fiestaseafood.com"                          # This may be the same as your `Web` server
#role :db,  "fiestaseafood.com", :primary => true # This is where Rails migrations will run

role :web, "113.20.31.104"
role :app, "113.20.31.104"
role :db, "113.20.31.104", :primary => true

set :user, "root"
#set :user, "fiestase"

default_run_options[:pty] = true

# if you want to clean up old releases on each deploy uncomment this:
#after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
end